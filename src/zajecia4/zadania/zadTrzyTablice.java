package zajecia4.zadania;

/* do tablicy x liczb podanych przez uzytkownika wylosowac liczby i je przypisac do tablicy
   nastepnie wydrukowac na konsoli liczby z danego przedzialu np. [4; 15) jesli istnieja
 */

import java.util.Scanner;

public class zadTrzyTablice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy: ");
        int rozmiar = scanner.nextInt();
        int[] tablica = new int[rozmiar];

        // wpisywanie wartosci przez uzytkownaika
        // tyle ile tablica ma elementow,
        // tyle razy prosimy o podanie liczby
        for (int i = 0; i < tablica.length; i++) {
            // do i-tego elementu tablicy wpisz wartosc od uzytkownika
            System.out.println("Podaj " + (i + 1) + " wartość: ");
            tablica[i] = scanner.nextInt();
        }
        // wyswietlanie wartosci - wyswietl tylko,
        // jezeli wartosc jest z przedziali 4 (wlacznie) do 14 (wlacznie)
        for (int i = 0; i < tablica.length; i++) {
            // wypisz tylko jezeli jest w okreslonym przedziale
            if (tablica[i] >= 4 && tablica[i] < 15) {
                System.out.println("Indeks: " + i + ", wartosc: " + tablica[i]);
            }
        }
    }
}
