package zajecia4.zadania;

/*
    Napisz program wczytujący z klawiatury n liczb całkowitych. Liczbę n należy pobrać od
    użytkownika. Jeśli podana wartość jest z zakresu 1-30, wówczas należy pobrać podaną ilość
    liczb całkowitych, a następnie wydrukować każdą z liczb podniesioną do kwadratu na
    ekranie. Jeśli liczba jest spoza tego przedziału należy zakończyć pracę drukując stosowny
    komunikat.
 */

import java.util.Scanner;

public class zadDwaTablice {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z zakresu 1-30: ");
        int rozmiar = sc.nextInt();

        if (rozmiar >= 1 && rozmiar <= 30) {
            int[] tab = new int[rozmiar];
            for (int i = 0; i <= rozmiar; i++) {
                tab[i] = i * i;
                System.out.println(tab[i]);
            }
        } else {
            System.out.println("Podana wartosc nie miesci sie w podanym przedziale 1-30.");
        }


    }
}
