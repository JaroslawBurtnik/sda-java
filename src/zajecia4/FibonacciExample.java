package zajecia4;

// napisac program wypisujacy n-ty wyraz ciagu Fibonacciego

import java.util.Scanner;

public class FibonacciExample {
    public static void main(String[] args) {
        // pobrac n z klawiatury
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj numer wyrazu ciagu Fibonacciego: ");
        int n = sc.nextInt();

        // wywolac funkcje fibonacci z klasy MathHelper i wydrukowac wynik
        System.out.println("Wynik to: " + MathHelper.fibonacci(n));
        }
    }

