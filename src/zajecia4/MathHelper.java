package zajecia4;

// Klasa pomocnicza z obliczeniami matematycznymi
public class MathHelper {

    /**
     * Metoda obliczajaca n! - to jest przyklad komentarza dokumentacyjnego pojawi sie po wcisnieciu CTRL + Q
     */
    public static long factorial(long n) {
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    /**
     * Metoda obliczajaca pole trojkata wg wzoru Herona
     *
     * @param a bok trojkata
     * @param b bok trojkata
     * @param c bok trojkata
     * @return pole trojkata
     */
    public static double obliczPole(double a, double b, double c) {
        double p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
        // alternatywa (nadmiarowa - wiecej linii kodu):
        // double wynik = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        // return wynik;
    }

    /**
     * Metoda obliczajaca n-ty wyraz ciagu Fibonacciego (iteracyjnie)
     */
    public static int fibonacci(int n) {
        int fib1 = 0;
        int fib2 = 1;
        int wynik = 0;

        for (int i = 2; i <= n; i++) {
           wynik = fib1 + fib2;
           fib1 = fib2;
           fib2 = wynik;
        }
        return wynik;
    }


}
