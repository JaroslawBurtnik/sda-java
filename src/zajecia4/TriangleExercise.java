package zajecia4;

import java.util.Scanner;

public class TriangleExercise {

    public static void main(String[] args) {

        Scanner insertValue = new Scanner(System.in);

        System.out.println("Podaj wartość boku \'a\' trójkąta: ");
        double a = insertValue.nextDouble();

        System.out.println("Podaj wartość boku \'b\' trójkąta: ");
        double b = insertValue.nextDouble();

        System.out.println("Podaj wartość boku \'c\' trójkąta: ");
        double c = insertValue.nextDouble();

//        double p = 0.5 * (a + b + c);
//        double poleTrojkata = Math.sqrt(p * (p - a) * (p - b) * (p - c));

        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            System.out.println("Mozna zbudowac trojkat.");
            double pole = MathHelper.obliczPole(a, b, c);
            System.out.println("Pole trojkata wynosi: " + pole);
            System.out.println(String.format("Pole trojkata wynosi: %.2f", pole));
//            %.2f - zaokraglenie do dwoch miejsc po przecinku
//            f tutaj oznacza, ze chodzi o zmienna zmiennoprzecinkowa, a nie float - jest to funkcja zapozyczona z C

        } else {
            System.out.println("Nie mozna zbudowac trojkata.");
        }
    }
}
