package zajecia4;

public class MethodOverview {
    public static void wypisz(String a, String b) {
        System.out.println((a + b));
    }

    // nie moze byc metody o tej samej sygnaturze
    // sygnatura to nazwa metody i argumenty jakie przyjmuje
    // sygnatura: ... wypisz(String a, String b)

    public static void main(String[] args) {
        wypisz("Jan", "Kowalski");
    }
}
