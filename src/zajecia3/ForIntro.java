package zajecia3;

public class ForIntro {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i += 2) {  // warkunek jest sprawdzany na samym pcczatku - tak jak w petli while
            System.out.println(i);
        }

        System.out.println("Liczby rosnaco.");
        for (int i = 0; i <= 500; i += 5) {
            System.out.println(i);
        }

        System.out.println("Liczby malejaco.");
        for (int i = 500; i >= 0; i -= 5) {
            System.out.println(i);
        }

        // petla nieskonczona:
//        dopoki prawda...
//        while(true) {
//
//        }

        // kolejny przyklad petli nieskonczonej
//        for (;;) {
//
//        }


    }
}
