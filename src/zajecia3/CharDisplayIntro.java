package zajecia3;

//  Wyswietlic znaki od A do Z
//  z uzyciem petli for
public class CharDisplayIntro {
    public static void main(String[] args) {
        System.out.println("For:");
        for(char c = 'A'; c <= 'Z'; c++) {
            System.out.println("Pozycja: " + (short)c + " znak: " + c);
        }
        System.out.println();
        System.out.println("While:");
        char c = 'a';
        while(c <= 'z') {
            System.out.println(c);
            c++;
        }
    }
}
