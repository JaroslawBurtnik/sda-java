package zajecia3.zadania;

//        Napisać program zliczający długość łańcucha znakowego podanego przez uzytkownika:
//        1) Z białymi znakami
//        2) Bez białych znaków (bez spacji i tabulatora)

import java.util.Scanner;

public class PetleZad4 {
    public static void main(String[] args) {

        // Utworzyc scanner
        Scanner sc = new Scanner(System.in);

        // Zapytac o napis
        System.out.println("Wpisz tekst i nacisnij enter: ");

        // Pobrac linie tekstu
        String line = sc.nextLine();

        // Wyswietlic menu:
//        1. Z bialymi znakami
//        2. Z bialymi znakami (metoda 2)
//        3. Bez bialych znakow
        System.out.println("1. Policz dlugosc z bialymi znakami.");
        System.out.println("2. Policz dlugosc bez bialych znakow.");

        int wybor = sc.nextInt();

        switch (wybor) {
            case 1:{
                int licznik = 0;
                for(int i=0; i < line.length(); i++) {
                    licznik++;
                    // lub po prostu System.out.println(line.length());
                }
                System.out.println(licznik);
                break;
            }

            case 2:{
                int licznik = 0;
                for(int i = 0; i < line.length(); i++) {
                    // jezeli znak na i-tej pozycji nie jest bialym znakiem to zlicaj
                    if (line.charAt(i) !=' ' && line.charAt(i) != '\t') {
                        licznik++;
                    }
                }
                System.out.println(licznik);
            }
                break;
            default:
                System.out.println("Zly wybor");
                break;
        }
        }
    }

