package zajecia3.zadania;

import java.util.Scanner;

public class PrimeChecker {

    public static boolean isPrime(int number) {
        boolean result = true;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                result = false;
                break;
            }
        }

        return result;
    }

    public static void main(String[] args) {

        System.out.println("Podaj liczbe: ");
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();
        boolean wynik = isPrime(liczba);
        System.out.println("Czy pierwsza? : " + liczba + " : " + wynik);

    }
}
