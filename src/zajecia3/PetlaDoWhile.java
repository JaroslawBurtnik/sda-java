package zajecia3;

// od 0 - 100
// wypisac nieparzyste z zakresu 0 - 100 z uzyciem do while
public class PetlaDoWhile {
    public static void main(String[] args) {

        int licznik = 0; // jesli int licznik = 101 to wynik koncowy wyswietlony bedzie tylko 101
                         // roznica polega miedzy dowhile a while, ze dowhile warunek jest podany na koncu, a wiec kod zawsze co najmniej raz sie wykona
        do {
            if (licznik % 2 != 0) {
                System.out.println(licznik);
            }
            licznik++;
        } while (licznik < 100);
    }


}
