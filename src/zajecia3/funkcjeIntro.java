package zajecia3;

public class funkcjeIntro {

    public static void wypiszParzyste() {
        for (int i = 0; i < 100; i += 2) {
            System.out.println(i);
        }
    }

    // funkcja ktora wypisuje napis przyjety jako argument
    public static void wypiszNapis(String name) {
        System.out.println("Argument to: " + name);
    }

    // funkcja dodajaca dwie liczby
    public static int dodajLiczby(int a, int b) {
        System.out.println("Dodaje dwie liczby...");
        int wynik = a + b;
        return wynik;
    }


    public static void main(String[] args) {
        wypiszParzyste();
        wypiszNapis("to jest przykład");

        int pierwszaLiczba = 2;
        int drugaLiczba = 5;
        int result = dodajLiczby(pierwszaLiczba, drugaLiczba);
        System.out.println(result);
    }
}
