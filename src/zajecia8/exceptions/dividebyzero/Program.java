package zajecia8.exceptions.dividebyzero;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwsza liczbe: ");
        int liczba = scanner.nextInt();
        System.out.println("Podaj druga liczbe: ");
        int drugaLiczba = scanner.nextInt();

        try {
            int result = liczba / drugaLiczba;
            System.out.println("Wynik: " + result);
        } catch (ArithmeticException ex) {
            System.out.println("Nie można dzielić przez zero!");
        }
    }
}

