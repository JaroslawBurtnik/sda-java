package homework;

//        Utwórz dwie macierze losowe A i B o wartościach z przedziału
//        〈4; 16〉 i wymiarach m x n, a następnie wykonaj operacje A+B i A-B . Wartość wczytaj od
//        użytkownika.
//        Napisz dwie funkcje do wczytywania i wypisywania na ekranie tablicy.

import java.util.Random;
import java.util.Scanner;

public class matrixExercise11 {

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i ++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf(matrix[i][j] + " | ");
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[][] matrixA;
        int[][] matrixB;

        int iloscWierszy;
        int iloscKolumn;
        System.out.println("Podaj wymiary tablic A i B.");
        System.out.println("Liczba wierszy: ");
        Scanner scanner = new Scanner(System.in);
        iloscWierszy = scanner.nextInt();
        System.out.println("Liczba kolumn: ");
        iloscKolumn = scanner.nextInt();

        matrixA = new int[iloscWierszy][iloscKolumn];
        matrixB = new int[iloscWierszy][iloscKolumn];

        Random losowanieLiczb = new Random();
        int wylosowanaLiczba = 0;

        // wypelnianie macierzy losowymi wartosciami
        for (int i = 0; i < matrixA.length; i++) {
            for (int j = 0; j < matrixA[i].length; j++) {
                matrixA[i][j] = losowanieLiczb.nextInt(16);
            }
        }

        for (int i = 0; i < matrixB.length; i++) {
            for (int j = 0; j < matrixB[i].length; j++) {
                matrixB[i][j] = losowanieLiczb.nextInt(16);
            }
        }

        // wyswietlanie macierzy
        printMatrix(matrixA);

        System.out.println();

        printMatrix(matrixB);




    }
}
