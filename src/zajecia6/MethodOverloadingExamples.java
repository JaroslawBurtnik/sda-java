package zajecia6;

public class MethodOverloadingExamples {

    public int add(int a, int b) {
        return a + b;
    }

    // metoda moze miec ta sama nazwe jesli ma inna sygnature
    // sygnatura w tym przypadku to czesc: ...add(int a, int b)
    // inna nazwa parametrow nie zmienia sygnatury - taka metoda nie bylaby wtedy przeciazeniem

    public double add(double a, double b) {
        return a + b;
    }
}
