package zajecia6.zadanie2;

public class Point {

    private double x;
    private double y;

    // przeciazaony konstruktor
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    // metoda obliczajaca odleglosc od srodka
    public double distanceFromOrigin() {
        return Math.sqrt((x * x + y * y));
    }



}
