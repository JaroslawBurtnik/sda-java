package zajecia6;

public class PetlaForEach {
    public static void main(String[] args) {

        int[] tab = new int[]{1, 2, 3, 4, 5, 6};

        // petla for
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }

        System.out.println();
        System.out.println("Uzycie petli for each: ");

        // petla for each
        for (int elem : tab) {
            System.out.print(elem + " ");
        }
    }
}
