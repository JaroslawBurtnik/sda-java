package zajecia7.inheritance;

//    Zaimplementuj klasy Person do reprezentacji osoby (z polami name,surname oraz age) oraz
//    klasy Student (z polami numer indeksu, kierunek, uczelnia), Pracownik (pola pensja,
//    stanowisko) oraz Pacjent (z polem typu tablica obiektow typu String).
//    Utworz kilka obiektow kazdego z tych typow i wywołaj ich metody


// Klasa bazowa - super class
public class Person {

    public Person() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    protected String name;
    private String surname;
    private int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String describe() {
        return name + " " + this.getSurname() + " " + this.getAge();
    }



}
