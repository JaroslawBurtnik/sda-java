package zajecia7.inheritance;

//    Zaimplementuj klasy Person do reprezentacji osoby (z polami name,surname oraz age) oraz
//    klasy Student (z polami numer indeksu, kierunek, uczelnia), Pracownik (pola pensja,
//    stanowisko) oraz Pacjent (z polem typu tablica obiektow typu String).
//    Utworz kilka obiektow kazdego z tych typow i wywołaj ich metody

public class Worker extends Person {
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    String position;
    double salary;

    public Worker(String name, String surname, int age, String position, double salary) {
        super(name, surname, age);
        this.position = position;
        this.salary = salary;
    }

    @Override
    public String describe(){
        return String.format("%s %s i jestem pracownikiem i pracuje jako: %s.", getName(), getSurname(), getPosition());
    }

    // dzieki przeciazeniu funkcji toString() mozemy wypisac sout(pracownik1)
    // i wyswietli nam tekst ponizej zamiast referencji do pamieci w systemie
    @Override
    public String toString(){
        return String.format("%s %s i jestem pracownikiem i pracuje jako: %s.", getName(), getSurname(), getPosition());
    }

}
