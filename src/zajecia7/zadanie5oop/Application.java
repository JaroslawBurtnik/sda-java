package zajecia7.zadanie5oop;

//        Zdefiniuj klasę Matrix do reprezentacji operacji na macierzach.
//        Zdefinuj konstruktor bezparametrowy, oraz konstruktor przyjmujący wymiary macierzy.
//        Aby można było dodawać macierze jej wymiary muszą być takie same.
//        Klasa powinna zawierać metody do dodawania oraz odejmowania macierzy.

public class Application {
    public static void main(String[] args) {

        Matrix first = new Matrix(3, 3);
        first.fillWithRandomValues();
        first.print();
        System.out.println();

        Matrix second = new Matrix(3, 3);
        second.fillWithRandomValues();
        second.print();
        System.out.println();
        System.out.println("Result: ");

        try {
            Matrix result = first.addMatrix(second);
            System.out.println(result);
            System.out.println(result.toString());
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }

    }
}
