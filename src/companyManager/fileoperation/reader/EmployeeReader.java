package companyManager.fileoperation.reader;
import companyManager.Employee;

/**
 * Interfejs okreslajacy metode odczytu pracownikow
 * Kazdy z typow czytnika ktory implementuje ten interfejs musi zaimplementowac ta metode
 */
public interface EmployeeReader {
    Employee[] readEmployees();
}
