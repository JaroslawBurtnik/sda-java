package zadania.instrukcje_warunkowe;

import java.util.Scanner;

public class TrojkatProstokatny {
    public static void main(String[] args) {
        int a;
        int b;
        int c;

        Scanner wstawWartosc = new Scanner(System.in);

        System.out.println("Podaj wartość boku \'a\' trójkąta: ");
        a = wstawWartosc.nextInt();

        System.out.println("Podaj wartość boku \'b\' trójkąta: ");
        b = wstawWartosc.nextInt();

        System.out.println("Podaj wartość boku \'c\' trójkąta: ");
        c = wstawWartosc.nextInt();

        int kwadratA = a * a;
        int kwadratB = b * b;
        int kwadratC = c * c;

        if(kwadratA + kwadratB == kwadratC) {
            System.out.println("Trójkąt jest prostokątny.");
        }
        else {
            System.out.println("Trójkąt nie jest prostokątny.");
        }

    }
}