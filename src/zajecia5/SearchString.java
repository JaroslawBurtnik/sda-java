package zajecia5;

import java.util.Scanner;

public class SearchString {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // Utworzyc tablice 5 napisow
        // i od razu ja zainicjalizowac
        String[] nazwiska = new String[5];

        // I wersja
        // Jezeli podamy zly indeks (za duzy lub za maly)
        // to bedzie rzucony wyjatek arrayIndexOut
//        nazwiska[0] = "Kowalski";
//        nazwiska[1] = "Nowak";
//        nazwiska[2] = "Adamiak";
//        nazwiska[3] = "Nowacki";
//        nazwiska[4] = "Kowal";

        // Blad zbyt duzy indeks!
//        nazwiska[9] = "Kowal";

        // II wersja - utworzenie tablicy 5-elementowej z inicjalizacja (szybsza i bezpieczniejsza wersja)
        // Rozmiar tablicy jest wywnioskowany po liczbie elementow podanych w nawiasach

        String[] surnames = {"Kowalski", "Nowak", "Adamiak", "Nowacki", "Kowal"};

        // Zapytac uzytkownika o nazwisko
        System.out.println("Podaj nazwisko: ");
        String nazwisko = sc.nextLine();
        boolean isPresent = false;
        for (int i = 0; i < surnames.length; i++) {
            if (surnames[i].equals(nazwisko)) {
                isPresent = true;
                break;
            }
            }
            // Odpowiedziec czy takie nazwisko wystepuje
        System.out.println(isPresent ? "Nazwisko wystepuje." : "Nazwisko nie wystepuje.");
        }
    }
