package zajecia1.operatory;

/*              Typy operatorów:
    == sprawdza równość
    != różny
    >= większy bądź równy
    <= mniejszy bądź równy
    >,< większy, mniejszy
    */

public class OperatoryPorownawcze {

    public static void main(String[] args){
        int a = 15;
        int b = 20;

        System.out.println("A = " + a + "B = " + b);

        // do zmiennej boolean o nazwie czyRowne spisz (operator = )
        // wynik operacji porwnania a == b

        boolean czyRowne = a == b;
        System.out.println("Czy \'a\' jest większe od \'b\'? " + "\t" + czyRowne);

        //czy rozne
        boolean czyRozne = a != b;
        System.out.println("Czy \'a\' różne od \'b\'/? " + "\t\t\t" + czyRozne);

        //czy a wieksze od b?
        boolean czyWieksze = a > b;
        System.out.println("Czy \'a\' większe od \'b\'? " + "\t\t" + czyWieksze);

        //czy a mniejsze od b?
        boolean czyMniejsze = a < b;
        System.out.println("Czy \'a\' mniejsze od \'b\'? " + "\t\t" + czyMniejsze);
    }
}
